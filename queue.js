let collection = [];

console.log(collection.length);


// Write the queue functions below.

// Output all the elements of the queue
function print() {
  return collection;
}

console.log(collection.length);


// Adds element to the rear of the queue
function enqueue(name, name2) {


    collection.push(name);
    console.log(collection)
    return collection;
}



// console.log(collection);

// console.log(collection.length);
// Removes element from the front of the queue
function dequeue() {
    // collection.push("Jane");
    // collection.shift();
    // console.log(collection)
    // return collection;
    collection.shift();
    return collection;
}


// Show element at the front
function front(name) {
    return collection[0];
}

// Show total number of elements
function size() {
    // console.log(collection);
   return collection.length;
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
    if(collection.length > 0){
        return false;
    }
    else {
        return true;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

